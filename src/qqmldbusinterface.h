#ifndef QQMLDBUSINTERFACE_H
#define QQMLDBUSINTERFACE_H

#include <QDBusConnection>
#include <QQmlParserStatus>
#include <QQmlPropertyMap>

#include "qqmldbusinterface_export.h"

class QDBusPendingCallWatcher;
class QQmlDBusInterfacePrivate;
class QQMLDBUSINTERFACE_EXPORT QQmlDBusInterface : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString service READ service WRITE setService NOTIFY serviceChanged)
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    Q_PROPERTY(QString iface READ interface WRITE setInterface NOTIFY interfaceChanged)
    Q_PROPERTY(QDBusConnection::BusType busType READ busType WRITE setBusType NOTIFY busTypeChanged)

public:
    explicit QQmlDBusInterface(const QString &service, const QString &path,
                               const QString &interface = QString(),
                               const QDBusConnection &connection = QDBusConnection::systemBus(),
                               QObject *parent = 0);
    virtual ~QQmlDBusInterface();

    QString service() const;
    void setService(const QString &service);

    QString path() const;
    void setPath(const QString &path);

    QString interface() const;
    void setInterface(const QString &interface);

    QDBusConnection::BusType busType() const;
    void setBusType(QDBusConnection::BusType busType);

    bool isValid() const;

Q_SIGNALS:
    void serviceChanged();
    void pathChanged();
    void interfaceChanged();
    void busTypeChanged();

private:
    Q_DECLARE_PRIVATE(QQmlDBusInterface)
    Q_DISABLE_COPY(QQmlDBusInterface)
    Q_PRIVATE_SLOT(d_func(), void _q_pendingWatcherFinished(QDBusPendingCallWatcher *watcher))
    Q_PRIVATE_SLOT(d_func(), void _q_processPropertiesChanged(const QString &interface,
                                                              const QVariantMap &changedProperties,
                                                              const QStringList &invalidatedProperties))
    Q_PRIVATE_SLOT(d_func(), void _q_forwardSignal(const QDBusMessage &message))

};

#endif
