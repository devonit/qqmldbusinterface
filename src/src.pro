DEPTH = ..
include($${DEPTH}/qqmldbusinterface.pri)

TEMPLATE = lib
TARGET = qqmldbusinterface
QT += dbus xml quick core-private dbus-private

# Input
HEADERS += \
    qqmldbusinterface_export.h \
    qqmldbusinterface_p.h \
    qqmldbusinterface.h

SOURCES += \
    qqmldbusinterface.cpp

INSTALL_HEADERS += \
    qqmldbusinterface_export.h \
    qqmldbusinterface.h

headers.files = $${INSTALL_HEADERS}
headers.path = $${PREFIX}/include/qqmldbusinterface
target.path = $${PREFIX}/$${LIBDIR}
INSTALLS += headers target

# pkg-config support
CONFIG += create_pc create_prl no_install_prl
QMAKE_PKGCONFIG_DESTDIR = pkgconfig
QMAKE_PKGCONFIG_LIBDIR = $$target.path
QMAKE_PKGCONFIG_INCDIR = $$headers.path
equals(QJSONRPC_LIBRARY_TYPE, staticlib) {
    QMAKE_PKGCONFIG_CFLAGS = -DQQMLDBUSINTERFACE_STATIC
} else {
    QMAKE_PKGCONFIG_CFLAGS = -DQQMLDBUSINTERFACE_SHARED
}
unix:QMAKE_CLEAN += -r pkgconfig lib$${TARGET}.prl
