#ifndef QQMLDBUSINTERFACE_P_H
#define QQMLDBUSINTERFACE_P_H

#include <private/qobject_p.h>
#include <private/qmetaobjectbuilder_p.h>
#include <private/qdbusintrospection_p.h>
#include <private/qdbusmessage_p.h>

#include <QDBusConnection>
#include <QDBusPendingCallWatcher>
#include <QJSValue>

class QQmlDBusInterface;
class QQmlDBusInterfaceMetaObject;
class QQmlDBusInterfacePrivate : public QObjectPrivate
{
public:
    QQmlDBusInterfacePrivate(const QString &s, const QString &p,
                             const QString &i, const QDBusConnection &c,
                             QQmlDBusInterface *iface);

    virtual ~QQmlDBusInterfacePrivate() {}

    // SLOTS
    void _q_pendingWatcherFinished(QDBusPendingCallWatcher *watcher);
    void _q_processPropertiesChanged(const QString &interface,
                                     const QVariantMap &changedProperties,
                                     const QStringList &invalidatedProperties);
    void _q_forwardSignal(const QDBusMessage &message);

    static void convertMessageValues(QVariantMap &map);
    static void convertMessageValues(QVariantList &list);
    static QVariant extractValueFromMessage(const QDBusMessage &message, int idx = 0);

    void configureInterface();
    QString introspect();
    QVariant property(const QByteArray &property);
    bool setProperty(const QByteArray &property, const QVariant &value);

    QQmlDBusInterface *q;
    QQmlDBusInterfaceMetaObject *metaObject;
    QDBusConnection::BusType busType;
    mutable QDBusConnection connection;
    QString service;
    QString path;
    QString interface;
    mutable QDBusError lastError;
    int timeout;
    bool valid;

    QHash<QDBusPendingCallWatcher*, QJSValue> pendingCalls;
};

class QQmlDBusInterfaceMetaObject : public QAbstractDynamicMetaObject
{
public:
    QQmlDBusInterfaceMetaObject(QQmlDBusInterface *qq, QQmlDBusInterfacePrivate *dd,
                                const QMetaObject *mo);
    ~QQmlDBusInterfaceMetaObject();

    void updateProperties(const QVariantMap &data);
    void dispatchSignal(const QDBusMessage &message);
    void initFromInterface(const QDBusIntrospection::Interface *iface);

private:
    static QByteArray dbusTypeName(const QString &dbusType);
    static QByteArray dbusTypeName(const QByteArray &dbusType);
    static QMetaType::Type dbusType(const QByteArray &dbusType);

    struct MetaSignalInfo {
        QString name;
        QByteArray signature;
        QList<QByteArray> parameterNames;
    };

    void addProperty(const QDBusIntrospection::Property property);
    MetaSignalInfo addSignal(const QDBusIntrospection::Signal &signal);
    void addMethod(const QDBusIntrospection::Method &method);

protected:
    virtual int metaCall(QMetaObject::Call _c, int _id, void **_a);
    virtual int createProperty(const char *name, const char *);

private:
    QQmlDBusInterface *q;
    QQmlDBusInterfacePrivate *d;

    QMetaObjectBuilder m_builder;
    QMetaObject *m_metaObject;
    QAbstractDynamicMetaObject *m_parent;

    int m_propertyOffset;
    int m_methodOffset;
    QHash<QByteArray, int> m_propertyIdLookup;
    QHash<QByteArray, QMetaType::Type> m_propertyTypeLookup;
    QHash<QByteArray, int> m_signalIdLookup;

    friend class QQmlDBusInterfacePrivate;
};

#endif
