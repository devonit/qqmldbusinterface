DEPTH = ../../..
include($${DEPTH}/qqmldbusinterface.pri)

TEMPLATE = app
TARGET = simple
INCLUDEPATH += $${QQMLDBUSINTERFACE_INCLUDEPATH}
LIBS += $${QQMLDBUSINTERFACE_LIBS}
unix:!macx:QMAKE_RPATHDIR += $${OUT_PWD}/$${DEPTH}/src

SOURCES += main.cpp
