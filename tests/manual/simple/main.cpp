#include <QCoreApplication>
#include "qqmldbusinterface.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

//    QQmlDBusInterface iface("org.freedesktop.NetworkManager",
//                            "/org/freedesktop/NetworkManager/Settings",
//                            "org.freedesktop.NetworkManager.Settings");

    QQmlDBusInterface iface("com.devonit.appliance.settings",
                            "/Database",
                            "com.devonit.appliance.settings.database");

    return 0;
}
