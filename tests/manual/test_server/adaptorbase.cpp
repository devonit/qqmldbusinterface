#include <QDebug>
#include "adaptorbase.h"

AdaptorBase::AdaptorBase(QObject *parent)
    : QDBusAbstractAdaptor(parent)
{
}

QString AdaptorBase::testProperty() const
{
    return m_testProperty;
}

void AdaptorBase::setTestProperty(const QString &value)
{
    m_testProperty = value;
}

void AdaptorBase::slotMethodFromBase(const QString &data)
{
    qDebug() << "data: " << data;
}
