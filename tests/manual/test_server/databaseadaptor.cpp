/*
 * This file was generated by qdbusxml2cpp version 0.8
 * Command line was: qdbusxml2cpp -a generated-adaptors/databaseadaptor -N -c DatabaseAdaptor dbus/com.devonit.appliance.settings.database.xml
 *
 * qdbusxml2cpp is Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
 *
 * This is an auto-generated file.
 * Do not edit! All changes made to it will be lost.
 */

#include <QtCore/QMetaObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QDebug>

#include "databaseadaptor.h"

DatabaseAdaptor::DatabaseAdaptor(QObject *parent)
  : AdaptorBase(parent),
    m_databaseDriver("testDriver"),
    m_hostIP("0.0.0.0"),
    m_hostPort("3306"),
    m_name("testName"),
    m_password("testPassword"),
    m_username("testUsername"),
    m_variant(42)
{
    m_stringListProperty << "surprise" << "i" << "am" << "jose" << "greco";
    m_variantMap.insert("test", "property");
    m_variantMap.insert("another", "test");
    m_variantMap.insert("number", 31337);

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(testSignal()));
    timer->start(5000);
}

DatabaseAdaptor::~DatabaseAdaptor()
{
}

void DatabaseAdaptor::propertyChanged(const char *p)
{
    // mega hacky, put into a template eventually
    QString propertyName = QString::fromUtf8(p);
    propertyName.remove("set");
    propertyName[0] = propertyName[0].toLower();

    QVariantMap properties;
    properties.insert(propertyName, property(propertyName.toLatin1().constData()));
    QString interfaceName = this->metaObject()->classInfo(0).value();
    Q_EMIT PropertiesChanged(interfaceName, properties, QStringList());
}

QString DatabaseAdaptor::databaseDriver() const
{
    return m_databaseDriver;
}

void DatabaseAdaptor::setDatabaseDriver(const QString &value)
{
    m_databaseDriver = value;
    propertyChanged(__func__);
}

QString DatabaseAdaptor::hostIP() const
{
    qDebug() << Q_FUNC_INFO;
    return m_hostIP;
}

void DatabaseAdaptor::setHostIP(const QString &value)
{
    m_hostIP = value;
    propertyChanged(__func__);
}

QString DatabaseAdaptor::hostPort() const
{
    qDebug() << Q_FUNC_INFO;
    return m_hostPort;
}

void DatabaseAdaptor::setHostPort(const QString &value)
{
    m_hostPort = value;
    propertyChanged(__func__);
}

QString DatabaseAdaptor::name() const
{
    qDebug() << Q_FUNC_INFO;
    return m_name;
}

void DatabaseAdaptor::setName(const QString &value)
{
    qDebug() << Q_FUNC_INFO << ": " << value;
    m_name = value;
    propertyChanged(__func__);
}

QString DatabaseAdaptor::password() const
{
    qDebug() << Q_FUNC_INFO;
    return m_password;
}

void DatabaseAdaptor::setPassword(const QString &value)
{
    qDebug() << Q_FUNC_INFO << ": " << value;
    m_password = value;
    propertyChanged(__func__);
}

QString DatabaseAdaptor::username() const
{
    qDebug() << Q_FUNC_INFO;
    return m_username;
}

void DatabaseAdaptor::setUsername(const QString &value)
{
    qDebug() << Q_FUNC_INFO << "value: " << value;
    m_username = value;
    propertyChanged(__func__);
}

QStringList DatabaseAdaptor::stringListProperty() const
{
    qDebug() << Q_FUNC_INFO;
    return m_stringListProperty;
}

void DatabaseAdaptor::setStringListProperty(const QStringList &value)
{
    qDebug() << Q_FUNC_INFO << "value: " << value;
    m_stringListProperty = value;
    propertyChanged(__func__);
}

QDBusVariant DatabaseAdaptor::variant() const
{
    return m_variant;
}

void DatabaseAdaptor::setVariant(const QDBusVariant &variant)
{
    m_variant = variant;
    propertyChanged(__func__);
}

QVariantMap DatabaseAdaptor::variantMap() const
{
    return m_variantMap;
}

void DatabaseAdaptor::setVariantMap(const QVariantMap &map)
{
    m_variantMap = map;
    propertyChanged(__func__);
}

void DatabaseAdaptor::apply()
{
}

void DatabaseAdaptor::load()
{
}

void DatabaseAdaptor::methodWithArguments(const QString &string, int integer)
{
    qDebug() << Q_FUNC_INFO << "args: " << string << ", " << integer;
}

bool DatabaseAdaptor::methodWithReturn()
{
    qDebug() << Q_FUNC_INFO;
    return false;
}

QString DatabaseAdaptor::methodWithReturnAndArguments(const QString &string)
{
    qDebug() << Q_FUNC_INFO << "value: " << string;
    return string + " world!";
}

QDBusVariant DatabaseAdaptor::methodThatReturnsAVariant()
{
    return QDBusVariant(42);
}

QVariantMap DatabaseAdaptor::methodThatReturnsAVariantMap()
{
    QVariantMap test;
    test.insert("hello", 42);
    test.insert("dogs", "cats");
    return test;
}

QVariantMap DatabaseAdaptor::methodThatTakesAndReturnsAVariantMap(const QVariantMap &data)
{
    return data;
}

void DatabaseAdaptor::testPropertiesChanged()
{
    setUsername("anotherUsername " + QString::number(qrand() % 255));
    setHostPort(QString::number(qrand() % 4000));
}

void DatabaseAdaptor::testSignal()
{
    qDebug() << Q_FUNC_INFO;
    Q_EMIT signalWithValue("test signal!");
}

void DatabaseAdaptor::testSignalWithNoValue()
{
    qDebug() << Q_FUNC_INFO;
    Q_EMIT signalWithNoValue();
}
