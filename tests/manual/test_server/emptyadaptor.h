#ifndef EMPTYADAPTOR_H
#define EMPTYADAPTOR_H

#include "emptyadaptorbase.h"

class EmptyAdaptor : public EmptyAdaptorBase
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "com.devonit.appliance.settings.emptyadaptorbase.emptyadaptor")

public:
    EmptyAdaptor(QObject *parent = 0);

};

#endif // EMPTYADAPTOR_H
