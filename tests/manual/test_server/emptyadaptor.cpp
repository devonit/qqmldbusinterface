#include "emptyadaptor.h"

EmptyAdaptor::EmptyAdaptor(QObject *parent)
    : EmptyAdaptorBase(parent)
{
    setObjectName("emptyadaptor");
    setEmptyAdaptorBaseProp("Set By EmptyAdaptor");
}
