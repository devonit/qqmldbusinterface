#ifndef ADAPTORBASE_H
#define ADAPTORBASE_H

#include <QDBusAbstractAdaptor>

class AdaptorBase : public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_PROPERTY(QString testProperty READ testProperty WRITE setTestProperty)
public:
    AdaptorBase(QObject *parent = 0);

    QString testProperty() const;
    void setTestProperty(const QString &value);

Q_SIGNALS:
    void signalFromBase();

public Q_SLOTS:
    void slotMethodFromBase(const QString &data);

private:
    QString m_testProperty;

};

#endif
