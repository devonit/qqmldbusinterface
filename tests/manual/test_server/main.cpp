#include <QCoreApplication>
#include "databaseadaptor.h"
#include "emptyadaptorbase.h"
#include "emptyadaptor.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    DatabaseAdaptor adaptor(&app);
    QDBusConnection dbus = QDBusConnection::systemBus();
    if (!dbus.registerService("com.devonit.appliance.settings")) {
        qDebug() << "\t" << "error registering service:" << dbus.lastError();
        return -1;
    }

    if (!dbus.registerObject("/Database", &app)) {
        qDebug() << "\t" << "error registering object:" << dbus.lastError();
        return -1;
    }

    EmptyAdaptorBase emptyAdaptorBase;
    EmptyAdaptor emptyAdaptor(&emptyAdaptorBase);
    if (!dbus.registerObject("/emptyadaptorbase", &emptyAdaptorBase,
                             QDBusConnection::ExportAdaptors |
                             QDBusConnection::ExportAllContents |
                             QDBusConnection::ExportChildObjects)) {
        qDebug() << "\t" << "error registering object: " << dbus.lastError();
        return -1;
    }


    return app.exec();
}
