TEMPLATE = app
TARGET = test_server
INCLUDEPATH += .
QT += dbus
unix:!macx:QMAKE_RPATHDIR += $${OUT_PWD}/$${DEPTH}/src

# Input
HEADERS += \
    adaptorbase.h \
    databaseadaptor.h \
    emptyadaptorbase.h \
    emptyadaptor.h

SOURCES += \
    adaptorbase.cpp \
    databaseadaptor.cpp \
    emptyadaptorbase.cpp \
    emptyadaptor.cpp \
    main.cpp

# install
dbus.files = com.devonit.appliance.conf
dbus.path = /etc/dbus-1/system.d
INSTALLS += dbus
