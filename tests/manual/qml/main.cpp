#include <QGuiApplication>
#include <QQuickView>
#include <QQmlContext>
#include <QQmlEngine>
#include <QQmlApplicationEngine>

#include "qqmldbusinterface.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine(QUrl("qrc:/qml/main.qml"));

    QQmlDBusInterface iface("com.devonit.appliance.settings",
                            "/Database",
                            "com.devonit.appliance.settings.database");

    QQmlDBusInterface emptyInterface("com.devonit.appliance.settings",
                                 "/emptyadaptorbase/emptyadaptor",
                                 "com.devonit.appliance.settings.emptyadaptorbase.emptyadaptor");

    engine.rootContext()->setContextProperty("databaseProxy", &iface);
    engine.rootContext()->setContextProperty("emptyInterface", &emptyInterface);
    return app.exec();
}
