DEPTH = ../../..
include($${DEPTH}/qqmldbusinterface.pri)

TEMPLATE = app
TARGET = qml
INCLUDEPATH += $${QQMLDBUSINTERFACE_INCLUDEPATH}
LIBS += $${QQMLDBUSINTERFACE_LIBS}
RESOURCES += qml.qrc
OTHER_FILES += \
    qml/main.qml
CONFIG += debug
unix:!macx:QMAKE_RPATHDIR += $${OUT_PWD}/$${DEPTH}/src

SOURCES += main.cpp
