#include <QEventLoop>
#include <QTimer>
#include <QJsonDocument>
#include <QDebug>

#include "helper.h"

InterfaceHelper::InterfaceHelper(QObject *parent)
    : QObject(parent),
      m_methodReturned(false)
{
}

InterfaceHelper::~InterfaceHelper()
{
}

void InterfaceHelper::reset()
{
    m_methodReturned = false;
    m_error.clear();
    m_result.clear();
}

bool InterfaceHelper::isError() const
{
    return m_error.isValid();
}

QVariant InterfaceHelper::result() const
{
    if (isError())
        return m_error;
    return m_result;
}

bool InterfaceHelper::waitForResult(int msecs)
{
    QEventLoop loop;
    connect(this, SIGNAL(callbackCalled()), &loop, SLOT(quit()));
    QTimer::singleShot(msecs, &loop, SLOT(quit()));
    loop.exec();

    if (m_methodReturned) {
        m_methodReturned = false;
        return true;
    }

    return false;
}

void InterfaceHelper::callback(const QVariant &error, const QVariant &result)
{
    m_error = error;
    m_result = result;
    m_methodReturned = true;
    Q_EMIT callbackCalled();
}

void InterfaceHelper::slotWithNoValue()
{
    m_methodReturned = true;
    m_result.clear();
    Q_EMIT callbackCalled();
}

void InterfaceHelper::slotWithStringValue(const QString &string)
{
    m_methodReturned = true;
    m_result = string;
    Q_EMIT callbackCalled();
}

void InterfaceHelper::slotWithVariantValue(const QDBusVariant &variant)
{
    m_methodReturned = true;
    m_result.setValue(variant);
    Q_EMIT callbackCalled();
}

void InterfaceHelper::slotWithVariantMapValue(const QVariantMap &map)
{
    m_methodReturned = true;
    m_result = map;
    Q_EMIT callbackCalled();
}
