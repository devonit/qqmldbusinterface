#include <QtCore/QMetaObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QDebug>

#include "adaptor.h"

TestAdaptor::TestAdaptor(QObject *parent)
  : QDBusAbstractAdaptor(parent),
    m_stringProperty("string"),
    m_intProperty(42),
    m_variantProperty(42)
{
    m_stringListProperty << "surprise" << "i" << "am" << "jose" << "greco";
    m_variantMapProperty.insert("test", "property");
    m_variantMapProperty.insert("another", "test");
    m_variantMapProperty.insert("number", 31337);

    {
        QVariantMap subSubVariantMap;
        subSubVariantMap.insert("hello", 42);
        subSubVariantMap.insert("dogs", "cats");

        QVariantMap subVariantMap;
        subVariantMap.insert("hello", 42);
        subVariantMap.insert("dogs", "cats");
        subVariantMap.insert("subSubVariantMap", subSubVariantMap);

        QVariantMap subVariantMap2;
        subVariantMap2.insert("hello", 42);
        subVariantMap2.insert("dogs", "cats");

        m_variantMapOfVariantMapsProperty.insert("hello", 42);
        m_variantMapOfVariantMapsProperty.insert("dogs", "cats");
        m_variantMapOfVariantMapsProperty.insert("subVariantMap", subVariantMap);
        m_variantMapOfVariantMapsProperty.insert("subVariantMap2", subVariantMap2);
    }

    {
        m_variantListProperty << QVariant(QString("TestString"));
        m_variantListProperty << QVariant(1);
        m_variantListProperty << QVariant(true);
        QVariantList nestedList;
        nestedList << QVariant(QString("NestedString"));
        nestedList << QVariant(2);
        m_variantListProperty << nestedList;
        QVariantMap nestedMap;
        nestedMap.insert("string", QString("NestedMapString"));
        nestedMap.insert("integer", QVariant(3));
        nestedMap.insert("boolean", QVariant(true));
        m_variantListProperty << nestedMap;
    }
}

TestAdaptor::~TestAdaptor()
{
}

void TestAdaptor::resetData()
{
    m_stringProperty = "string";
    m_intProperty = 42;
    m_variantProperty = QDBusVariant(42);
    m_stringListProperty = QStringList() << "surprise" << "i" << "am" << "jose" << "greco";

    m_variantMapProperty.clear();
    m_variantMapProperty.insert("test", "property");
    m_variantMapProperty.insert("another", "test");
    m_variantMapProperty.insert("number", 31337);
}

void TestAdaptor::propertyChanged(const char *p)
{
    // mega hacky, put into a template eventually
    QString propertyName = QString::fromUtf8(p);
    propertyName.remove("set");
    propertyName[0] = propertyName[0].toLower();

    QVariantMap properties;
    properties.insert(propertyName, property(propertyName.toLatin1().constData()));
    QString interfaceName = this->metaObject()->classInfo(0).value();
    Q_EMIT PropertiesChanged(interfaceName, properties, QStringList());
}

QString TestAdaptor::stringProperty() const
{
    return m_stringProperty;
}

void TestAdaptor::setStringProperty(const QString &value)
{
    m_stringProperty = value;
    propertyChanged(__func__);
}

int TestAdaptor::intProperty() const
{
    return m_intProperty;
}

void TestAdaptor::setIntProperty(int value)
{
    m_intProperty = value;
    propertyChanged(__func__);
}

QStringList TestAdaptor::stringListProperty() const
{
    return m_stringListProperty;
}

void TestAdaptor::setStringListProperty(const QStringList &value)
{
    m_stringListProperty = value;
    propertyChanged(__func__);
}

QDBusVariant TestAdaptor::variantProperty() const
{
    return m_variantProperty;
}

void TestAdaptor::setVariantProperty(const QDBusVariant &variant)
{
    m_variantProperty = variant;
    propertyChanged(__func__);
}

QVariantMap TestAdaptor::variantMapProperty() const
{
    return m_variantMapProperty;
}

void TestAdaptor::setVariantMapProperty(const QVariantMap &map)
{
    m_variantMapProperty = map;
    propertyChanged(__func__);
}

QVariantMap TestAdaptor::variantMapOfVariantMapsProperty() const
{
    return m_variantMapOfVariantMapsProperty;
}

void TestAdaptor::setVariantMapOfVariantMapsProperty(const QVariantMap &map)
{
    m_variantMapOfVariantMapsProperty = map;
    propertyChanged(__func__);
}

QVariantList TestAdaptor::variantListProperty() const
{
    return m_variantListProperty;
}

void TestAdaptor::setVariantListProperty(const QVariantList &list)
{
    m_variantListProperty = list;
}

void TestAdaptor::methodWithArguments(const QString &string, int integer)
{
    Q_UNUSED(string)
    Q_UNUSED(integer)
}

bool TestAdaptor::methodWithReturn()
{
    return true;
}

QString TestAdaptor::methodWithReturnAndArguments(const QString &string)
{
    return string + " world!";
}

QDBusVariant TestAdaptor::methodThatReturnsAVariant()
{
    return QDBusVariant(42);
}

QVariantMap TestAdaptor::methodThatReturnsAVariantMap()
{
    QVariantMap test;
    test.insert("hello", 42);
    test.insert("dogs", "cats");
    return test;
}

QVariantMap TestAdaptor::methodThatReturnsAVariantMapOfVariantMaps()
{
    QVariantMap subSubVariantMap;
    subSubVariantMap.insert("hello", 42);
    subSubVariantMap.insert("dogs", "cats");

    QVariantMap subVariantMap;
    subVariantMap.insert("hello", 42);
    subVariantMap.insert("dogs", "cats");
    subVariantMap.insert("subSubVariantMap", subSubVariantMap);

    QVariantMap subVariantMap2;
    subVariantMap2.insert("hello", 42);
    subVariantMap2.insert("dogs", "cats");

    QVariantMap result;
    result.insert("hello", 42);
    result.insert("dogs", "cats");
    result.insert("subVariantMap", subVariantMap);
    result.insert("subVariantMap2", subVariantMap2);
    return result;
}

QVariantMap TestAdaptor::methodThatTakesAndReturnsAVariantMap(const QVariantMap &data)
{
    return data;
}

QVariantList TestAdaptor::methodThatReturnsAVariantList()
{
    QVariantList result;
    result << QVariant(QString("TestString"));
    result << QVariant(1);
    result << QVariant(true);
    QVariantList nestedList;
    nestedList << QVariant(QString("NestedString"));
    nestedList << QVariant(2);
    result << nestedList;
    QVariantMap nestedMap;
    nestedMap.insert("string", QString("NestedMapString"));
    nestedMap.insert("integer", QVariant(3));
    nestedMap.insert("boolean", QVariant(true));
    result << nestedMap;
    return result;
}

void TestAdaptor::testPropertiesChanged()
{
    setStringProperty("anotherUsername " + QString::number(qrand() % 255));
    setIntProperty(qrand() % 4000);
}

void TestAdaptor::emitSignalWithNoValue()
{
    Q_EMIT signalWithNoValue();
}

void TestAdaptor::emitSignalWithStringValue()
{
    Q_EMIT signalWithStringValue("string");
}

void TestAdaptor::emitSignalWithVariantValue()
{
    Q_EMIT signalWithVariantValue(QDBusVariant(11.22));
}

void TestAdaptor::emitSignalWithVariantMapValue()
{
    QVariantMap map;
    map.insert("donkies", "will");
    map.insert("rule", "the");
    map.insert("modern", "world");
    Q_EMIT signalWithVariantMapValue(map);
}

