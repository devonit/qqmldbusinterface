#ifndef ADAPTOR_H
#define ADAPTOR_H

#include <QtCore/QObject>
#include <QtDBus/QtDBus>

QT_BEGIN_NAMESPACE
class QByteArray;
template<class T> class QList;
template<class Key, class Value> class QMap;
class QString;
class QStringList;
class QVariant;
QT_END_NAMESPACE

class TestAdaptor: public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "com.devonit.qqmldbusinterface.test")
    Q_PROPERTY(QString stringProperty READ stringProperty WRITE setStringProperty)
    Q_PROPERTY(int intProperty READ intProperty WRITE setIntProperty)
    Q_PROPERTY(QStringList stringListProperty READ stringListProperty WRITE setStringListProperty)
    Q_PROPERTY(QDBusVariant variantProperty READ variantProperty WRITE setVariantProperty)
    Q_PROPERTY(QVariantMap variantMapProperty READ variantMapProperty WRITE setVariantMapProperty)
    Q_PROPERTY(QVariantMap variantMapOfVariantMapsProperty READ variantMapOfVariantMapsProperty
                                                           WRITE setVariantMapOfVariantMapsProperty)
    Q_PROPERTY(QVariantList variantListProperty READ variantListProperty WRITE setVariantListProperty)
public:
    TestAdaptor(QObject *parent = 0);
    virtual ~TestAdaptor();

    QString stringProperty() const;
    void setStringProperty(const QString &value);

    int intProperty() const;
    void setIntProperty(int value);

    QStringList stringListProperty() const;
    void setStringListProperty(const QStringList &value);

    QDBusVariant variantProperty() const;
    void setVariantProperty(const QDBusVariant &variant);

    QVariantMap variantMapProperty() const;
    void setVariantMapProperty(const QVariantMap &map);

    QVariantMap variantMapOfVariantMapsProperty() const;
    void setVariantMapOfVariantMapsProperty(const QVariantMap &map);

    QVariantList variantListProperty() const;
    void setVariantListProperty(const QVariantList &list);

public Q_SLOTS: // METHODS
    void methodWithArguments(const QString &string, int integer);
    bool methodWithReturn();
    QString methodWithReturnAndArguments(const QString &string);

    QDBusVariant methodThatReturnsAVariant();
    QVariantMap methodThatReturnsAVariantMap();
    QVariantMap methodThatReturnsAVariantMapOfVariantMaps();
    QVariantMap methodThatTakesAndReturnsAVariantMap(const QVariantMap &data);
    QVariantList methodThatReturnsAVariantList();

    void emitSignalWithNoValue();
    void emitSignalWithStringValue();
    void emitSignalWithVariantValue();
    void emitSignalWithVariantMapValue();

Q_SIGNALS: // SIGNALS
    void PropertiesChanged(const QString &interface_name,
                           const QVariantMap &changed_properties,
                           const QStringList &invalidated_properties);

    void signalWithNoValue();
    void signalWithStringValue(const QString &string);
    void signalWithVariantValue(const QDBusVariant &variant);
    void signalWithVariantMapValue(const QVariantMap &variantMap);

private Q_SLOTS:
    void testPropertiesChanged();

private:
    void resetData();
    void propertyChanged(const char *p);

    QString m_stringProperty;
    int m_intProperty;
    QStringList m_stringListProperty;
    QDBusVariant m_variantProperty;
    QVariantMap m_variantMapProperty;
    QVariantMap m_variantMapOfVariantMapsProperty;
    QVariantList m_variantListProperty;

    friend class tst_QQmlDBusInterface;
};

#endif
