#ifndef HELPER_H
#define HELPER_H

#include <QVariant>
#include <QDBusVariant>

class InterfaceHelper : public QObject
{
    Q_OBJECT
public:
    explicit InterfaceHelper(QObject *parent = 0);
    ~InterfaceHelper();

    void reset();

    bool waitForResult(int msecs = 30000);
    bool isError() const;
    QVariant result() const;

Q_SIGNALS:
    void callbackCalled();

public Q_SLOTS:
    void callback(const QVariant &error, const QVariant &result);

    void slotWithNoValue();
    void slotWithStringValue(const QString &string);
    void slotWithVariantValue(const QDBusVariant &variant);
    void slotWithVariantMapValue(const QVariantMap &map);

private:
    QVariant m_error;
    QVariant m_result;
    bool m_methodReturned;

};

#endif
