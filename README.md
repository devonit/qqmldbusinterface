Overview
========

QQmlDBusInterface is a library for exposing DBus interfaces in QML.
QQmlDBusInterface is licensed under the LGPLv2.1

Requirements
============

- Qt 5.2 or higher
- Qt5 private headers
- dbus

Features
========

- DBus interaction directly from QML
- DBus interfaces exposed as plain javascript objects
- Asynchronous function calls

Building
========

    mkdir build
    cd build
    qmake ..
    make && make intall

You can pass the following arguments to qmake:
    PREFIX=<prefix>                  to change the install prefix
    QQMLDBUSINTERFACE_LIBRARY_TYPE=staticlib  to build a static version of the library

